// Initial modules
const { src, dest, watch, series, parallel } = require('gulp');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const concat = require('gulp-concat');
const postcss = require('gulp-postcss');
const rename = require('gulp-rename');
const replace = require('gulp-replace');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const uglify = require('gulp-uglify');

// File path variables
const files = {
  scssPath: 'dev/sass/main.scss',
  jsPath: 'dev/js/app.js',

  cssMinPath: 'dev/css/main.min.css',
  jsMinPath: 'dev/js/app.min.js'
}

// Sass task
function scssTask() {
  return src(files.scssPath)
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(postcss([ autoprefixer('last 2 versions'), cssnano() ]))
    .pipe(sourcemaps.write('.'))
    .pipe(dest('public/css')
  );
}

// JS Task
function jsTask() {
  return src(files.jsPath)
    .pipe(concat('app.js'))
    .pipe(uglify())
    .pipe(dest('public/js')
  );
}

// Cachebusting task
const cbString = new Date().getTime();
function cacheBustingTask() {
  return src(['index.html'])
    .pipe(replace(/cb=\d+/g, 'cb=' + cbString))
    .pipe(dest('.')
  );
}

// Watch task
function watchTask() {
  watch([files.scssPath, files.jsPath],
    parallel(scssTask, jsTask));
}

// Default task
exports.default = series(
  parallel(scssTask, jsTask),
  cacheBustingTask,
  watchTask
);

// Move styles to public
function styles() {
  return src(files.cssMinPath)
    .pipe(dest('./public/css')
  );
}

// Move scripts to public 
function scripts() {
  return src(files.jsMinPath)
    .pipe(dest('./public/js')
  );
}

// Move to public folder task
exports.build = series(
  styles,
  scripts
);